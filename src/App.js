import React from "react";
import List from "./components/List";
import  ListCopy  from "./components/ListCopy";

function App() {
  return (
    <div className="App" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-around'}}>
      <List />
      <ListCopy />
    </div>
  );
}

export default App;
