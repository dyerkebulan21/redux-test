import listReducer from './list'

import {combineReducers} from 'redux'

const allReducers = combineReducers({
    listReducer
})

export default allReducers