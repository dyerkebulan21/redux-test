import React from "react";
import { connect } from "react-redux";

const ListCopy = props => {
  return (
    <div>
      <h3>Hello</h3>
      <div style={{ display: "flex", flexDirection: "column" }}>
        {props.list.map(el => (
          <span style={{ paddingBlockStart: "1em" }}>{el}</span>
        ))}
      </div>
    </div>
  );
};

export default connect(
  state => ({
    list: state.listReducer
  })
)(ListCopy)