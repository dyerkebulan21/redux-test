import React from "react";
import { connect } from "react-redux";
// import { listAction } from "../actions";

const data = [
  "Lorem ipsum 1",
  "Lorem ipsum 2",
  "Lorem ipsum 3",
  "Lorem ipsum 4",
  "Lorem ipsum 5",
  "Lorem ipsum 6"
];

const List = props => {
  return (
    <div>
      <h3>Hello</h3>
      <div style={{ display: "flex", flexDirection: "column" }}>
        {data.map(el => (
          <span
            onClick={() => {
              props.listAction(el);
            }}
            style={{ paddingBlockStart: "1em" }}
          >
            {el}
          </span>
        ))}
      </div>
    </div>
  );
};

const mapDispatch = dispatch => ({
  listAction: el => dispatch({ type: "ADD_LIST", payload: el })
});

export default connect(
  state => ({
    list: state.listReducer
  }),
  mapDispatch
)(List);
